---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Eurobot - Système de localisation basé sur la vision par ordinateur
filières:
  - Informatique
nombre d'étudiants: 1
mots-clés:
  - Eurobot
  - Robotique
  - Systèmes embarqués
  - Traitement d’image
  - Vision par ordinateur
langue: [F,E,D]
confidentialité: non
suite: non
attribué à: ["Denis Rosset"]
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/barad-dur.jpg}
\end{center}
```

## Description/Contexte

Depuis plusieurs années, notre école participe au concours de robotique Eurobot.
Ce concours réunit des étudiants des filières informatique, génie électrique et
génie mécanique, dans le but de réaliser deux robots capables d'effectuer
différentes tâches.
La nécessité de connaitre la position des robots (alliés et adverses) est
primordiale afin de prévoir les déplacements et les actions à effectuer.

La filière informatique a déjà développé plusieurs solutions pour connaitre
la position des robots sur le plateau, mais ces méthodes souffraient de
limites en termes d'utilisation et de précision.
Le but de ce projet est de créer un système de localisation à partir d’une
caméra posée sur un mât au-dessus du plateau et de marqueurs présents sur les
robots. L’étudiant qui choisit ce projet aura l’occasion
d’implémenter un système embarqué performant en expérimentant avec des langages,
du matériel embarqué et des concepts de traitement d’image.

## Objectifs/Tâches

- Analyse de matériel embarqué et de caméra adéquates
- Choix d’un système embarqué et des technologies logicielles pour ce projet
- Développement d’un système sufissement précis pour pouvoir localiser
  les ennemis et capable de calibrer le robot pendant un match
- Tests et validation du système
- Rédaction d'une documentation adéquate.
