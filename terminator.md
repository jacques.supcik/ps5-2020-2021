---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Contrôleur de bras robotique par WebUSB
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
mots-clés: [Web, WebUSB, Robotique, Servos, Systèmes embarqués]
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/terminator.jpg}
\end{center}
```

## Description/Contexte

Depuis quelques années, les navigateurs Chrome, Edge, et Opera implémentent l'interface WebUSB (`https://developer.mozilla.org/en-US/docs/Web/API/USB`).
Cette interface permet à une page Web de contrôler un appareil connecté au port USB d'un ordinateur sans rien avoir à installer d'autre qu'un navigateur Web.

Le but de ce projet est de contrôler un bras robotique, actionné par 4 servos, depuis une simple page Web.
Il faudra pour cela réaliser un système embarqué qui contrôlera les servos selon les instructions reçues par son port USB. Les "boards" de type "bluepill" à base de STM32 ou l'Arduino "Leonardo" (voir projet `https://experiments.withgoogle.com/tiny-sorter`) sont des candidats potentiels pour le hardware, mais l'étudiant qui choisit ce projet aura la possibilité de proposer autre chose.

## Objectifs/Tâches

- Etude de l'API WebUSB.
- Analyse des "boards" capable d'implémenter un appareil compatible avec WebUSB.
- Choix d'un système embarqué et des technologies logicielles pour ce projet.
- Implémentation d'un prototype fonctionnel.
- Implémentation d'une page Web simple qui permet de contrôler le bras robotique.
- Tests et validation du système.
- Rédaction d'une documentation adéquate.