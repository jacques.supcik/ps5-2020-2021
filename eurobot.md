---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Eurobot Horizon 2022
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 2
mots-clés: [Eurobot, Robotique, Raspberrry Pi, Systèmes embarqués]
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/rtfm.jpg}
\end{center}
```

## Description/Contexte

Depuis plusieurs années, notre école participe au concours de robotique Eurobot. Ce concours
réunit des étudiants des filières informatique, génie électrique et génie mécanique. Depuis
plusieurs années, l'équipe de la HEIA-FR utilise des programmes écrits en C++ et en Python
qui ont évolué d'année en année.

Chaque année, les équipes améliorent le code dans le but de gagner le concours et elles n'ont pas
le temps de repenser le code dans son ensemble. Le but de ce projet est de faire une toute nouvelle
conception du code des robots sans avoir la pression du concours. L'étudiant qui choisit ce projet
aura l'occasion d'expérimenter des concepts, des langages, et des librairies modernes sur
une plateforme originale et performante.

## Objectifs/Tâches

- Etude du code existant des robots de l'équipe de la HEIA-FR.
- Analyse des capteurs, des actuateurs, et des interfaces des robots existants
- Analyse des langages, et des librairies modernes pour contrôler efficacement les robots.
- Choix d'un système embarqué et des technologies logicielles pour ce projet.
- Implémentation d'un prototype fonctionnel.
- Tests et validation du système.
- Rédaction d'une documentation adéquate.
